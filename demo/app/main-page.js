var Observable = require('data/observable').Observable;
var rootBeer = require('nativescript-rootbeer');

var pageData = new Observable();
var methods = [
    'isRooted',
    'detectRootManagementApps',
    'detectPotentiallyDangerousApps',
    'detectTestKeys',
    'checkForBusyBoxBinary',
    'checkForSuBinary',
    'checkSuExists',
    'checkForRWPaths',
    'checkForDangerousProps',
    'checkForRootNative',
    'detectRootCloakingApps'
];

methods.forEach(function(method){
    console.log(method);
    pageData[method] = rootBeer[method]() ? 'Yes' : 'No';
});

function onNavigatingTo(args) {
    var page = args.object;
    page.bindingContext = pageData;
}

exports.onNavigatingTo = onNavigatingTo;