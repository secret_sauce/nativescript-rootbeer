# NativeScript RootBeer

A [NativeScript](https://nativescript.org/) module for detecting root on Android. It uses the [RootBeer](https://github.com/scottyab/rootbeer) library

## Installation
From the command prompt go to your app's root folder and execute:
```
tns plugin add nativescript-rootbeer
```

## Usage

Here are the supported functions:

### function: isRooted
JavaScript

```js
  let rootBeer = require('nativescript-rootbeer')

  rootBeer.isRooted();
```
TypeScript

```ts
  import { isRooted } from 'nativescript-rootbeer'

  isRooted()
```

### function: detectRootManagementApps
JavaScript

```js
  let rootBeer = require('nativescript-rootbeer')

  rootBeer.detectRootManagementApps();
```
TypeScript

```ts
  import { detectRootManagementApps } from 'nativescript-rootbeer'

  detectRootManagementApps()
```

### function: detectPotentiallyDangerousApps
JavaScript

```js
  let rootBeer = require('nativescript-rootbeer');

  rootBeer.detectPotentiallyDangerousApps();
```
TypeScript

```ts
  import { detectPotentiallyDangerousApps } from 'nativescript-rootbeer'

  detectPotentiallyDangerousApps()
```

### function: detectTestKeys
JavaScript

```js
  let rootBeer = require('nativescript-rootbeer');

  rootBeer.detectTestKeys();
```
TypeScript

```ts
  import { detectTestKeys } from 'nativescript-rootbeer'

  detectTestKeys()
```

### function: checkForBusyBoxBinary
JavaScript

```js
  let rootBeer = require('nativescript-rootbeer');

  rootBeer.checkForBusyBoxBinary();
```
TypeScript

```ts
  import { checkForBusyBoxBinary } from 'nativescript-rootbeer'

  checkForBusyBoxBinary()
```

### function: checkForSuBinary
JavaScript

```js
  let rootBeer = require('nativescript-rootbeer');

  rootBeer.checkForSuBinary();
```
TypeScript

```ts
  import { checkForSuBinary } from 'nativescript-rootbeer'

  checkForSuBinary()
```

### function: checkSuExists
JavaScript

```js
  let rootBeer = require('nativescript-rootbeer');

  rootBeer.checkSuExists();
```
TypeScript

```ts
  import { checkSuExists } from 'nativescript-rootbeer'

  checkSuExists()
```

### function: checkForRWPaths
JavaScript

```js
  let rootBeer = require('nativescript-rootbeer');

  rootBeer.checkForRWPaths();
```
TypeScript

```ts
  import { checkForRWPaths } from 'nativescript-rootbeer'

  checkForRWPaths()
```

### function: checkForDangerousProps
JavaScript

```js
  let rootBeer = require('nativescript-rootbeer');

  rootBeer.checkForDangerousProps();
```
TypeScript

```ts
  import { checkForDangerousProps } from 'nativescript-rootbeer'

  checkForDangerousProps()
```

### function: checkForRootNative
JavaScript

```js
  let rootBeer = require('nativescript-rootbeer');

  rootBeer.checkForRootNative();
```
TypeScript

```ts
  import { checkForRootNative } from 'nativescript-rootbeer'

  checkForRootNative()
```

### function: detectRootCloakingApps
JavaScript

```js
  let rootBeer = require('nativescript-rootbeer');

  rootBeer.detectRootCloakingApps();
```
TypeScript


```ts
  import { detectRootCloakingApps } from 'nativescript-rootbeer'

  detectRootCloakingApps()
```

### Thanks

[Scott Alexander-Bown](https://github.com/scottyab) and [Matthew Rollings](https://github.com/stealthcopter) for their contributions to RootBeer


# License


Apache License, Version 2.0



    Copyright (C) 2015, Scott Alexander-Bown, Mat Rollings, Ryan Edge

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
