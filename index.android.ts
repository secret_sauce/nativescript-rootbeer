import { android } from 'application'

let _context
let _rootbeer

declare var com

let getContext = () => {
    if (!_context) {
        _context = android.context
    }

    return _context
}

let getRootBeer = () => {
    if (!_rootbeer) {
        _rootbeer = new com.scottyab.rootbeer.RootBeer(getContext())
    }

    return _rootbeer
}

export let isRooted = () => getRootBeer().isRooted()
export let detectRootManagementApps = () => getRootBeer().detectRootManagementApps()
export let detectPotentiallyDangerousApps = () => getRootBeer().detectPotentiallyDangerousApps()
export let detectTestKeys = () => getRootBeer().detectTestKeys()
export let checkForBusyBoxBinary = () => getRootBeer().checkForBusyBoxBinary()
export let checkForSuBinary = () => getRootBeer().checkForSuBinary()
export let checkSuExists = () => getRootBeer().checkSuExists()
export let checkForRWPaths = () => getRootBeer().checkForRWPaths()
export let checkForDangerousProps = () => getRootBeer().checkForDangerousProps()
export let checkForRootNative = () => getRootBeer().checkForRootNative()
export let detectRootCloakingApps = () => getRootBeer().detectRootCloakingApps()